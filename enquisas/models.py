from django.db import models
import datetime as dt
from django.contrib.auth.models import User

class Enquisa(models.Model):
    pregunta = models.CharField(max_length=200)
    data_creacion = models.DateTimeField(default=dt.datetime.now())
    dias_activo = models.IntegerField(default=3)
    usuarios_que_votaron = models.ManyToManyField(User)
    imaxe = models.ImageField(null=True, blank=True, upload_to='images/')

    def __str__(self):
        return f'{self.id} - {self.pregunta}'
    
class Opcion(models.Model):
    texto_opcion = models.CharField(max_length=100)
    enquisa = models.ForeignKey(Enquisa, on_delete=models.CASCADE)
    votos = models.IntegerField(default=0)

    def __str__(self) -> str:
        return f'{self.enquisa.pregunta} - {self.texto_opcion}'






