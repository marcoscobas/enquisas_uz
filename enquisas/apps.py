from django.apps import AppConfig


class EnquisasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'enquisas'
