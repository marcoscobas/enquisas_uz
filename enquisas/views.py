from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Enquisa, Opcion
from .forms import FormularioEnquisa, FormularioOpcion
from django.contrib import messages
from django.contrib.auth.decorators import login_required 

def home(request):
    context = {
        'lista_enquisas': Enquisa.objects.all()
    }

    return render(request, 'enquisas/lista_enquisas.html', context)


@login_required(login_url="/usuarios/login_user/")
def detalle_enquisa(request, id_enquisa):

    if request.method == 'POST':

        id_opcion = int(request.POST['opciones'])

        user = request.user
        opcion = Opcion.objects.get(pk=id_opcion)

        if user in opcion.enquisa.usuarios_que_votaron.all():
            messages.success(request, message='Síntoo, xa votaches nesta enquisa, non podes volver a votar.',
                             extra_tags='danger')
            return redirect('detalle_enquisa', opcion.enquisa.id)
        else:

            opcion.enquisa.usuarios_que_votaron.add(user)

            opcion.votos += 1
            opcion.save()

            context = {
                'opcion': opcion
            }
            
            return render(request,'enquisas/votar_opcion.html', context)
    
    else:

        context = {
            'enquisa': Enquisa.objects.get(pk=id_enquisa)
        }
        return render(request, 'enquisas/detalle_enquisa.html', context)


@login_required(login_url="/usuarios/login_user/")
def crear_enquisa(request):

    if request.method == 'POST': 
        form = FormularioEnquisa(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            messages.success(request, 'A enquisa creouse correctamente.', extra_tags='success')
            messages.success(request, 'Introduza outra enquisa.', extra_tags='info')
            return render(request, 'enquisas/nova_enquisa.html', {'form': FormularioEnquisa()})
        
    else:
        form = FormularioEnquisa()
        return render(request, 'enquisas/nova_enquisa.html', {'form': form})

@login_required(login_url="/usuarios/login_user/")
def crear_opcion(request):

    if request.method == 'POST': 
        form = FormularioOpcion(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, 'A opción creouse correctamente.', extra_tags='success')
            messages.success(request, 'Introduza outra opción.', extra_tags='info')
            return render(request, 'enquisas/nova_opcion.html', {'form': FormularioOpcion()})
        
    else:
        form = FormularioOpcion()
        return render(request, 'enquisas/nova_opcion.html', {'form': form})