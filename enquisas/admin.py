from django.contrib import admin
from .models import Enquisa, Opcion

admin.site.register(Enquisa)
admin.site.register(Opcion)
