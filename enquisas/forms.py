from django import forms
from django.forms import ModelForm

from .models import Enquisa, Opcion

class FormularioEnquisa(ModelForm):

    pregunta = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    dias_activo = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}))

    class Meta:
        model = Enquisa
        fields = ('pregunta', 'dias_activo', 'imaxe')
        

class FormularioOpcion(ModelForm):
    enquisa = forms.ModelChoiceField(Enquisa.objects.all(), widget=forms.Select(attrs={'class':'form-control'}))
    texto_opcion = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))

    class Meta:
        model = Opcion
        fields = ('enquisa', 'texto_opcion' )